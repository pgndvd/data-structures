package structures.queue;

public interface Queue<T> {

	public boolean isEmpty();

	public boolean isFull();

	public void enqueue(T element);

	public T dequeue();
}

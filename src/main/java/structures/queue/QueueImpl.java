package structures.queue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QueueImpl<T> implements Queue<T> {
	private int capacity;
	private T[] elements;
	private int headIndex;
	private int tailIndex;
	private int totalElements;

	public QueueImpl(int capacity) {
		this.capacity = capacity;
		elements = (T[]) new Object[capacity];
	}

	public boolean isEmpty() {
		if (totalElements == 0) {
			return true;
		}
		return false;
	}

	public boolean isFull() {
		if (totalElements == capacity) {
			return true;
		}
		return false;
	}

	// O(1)
	public void enqueue(T element) {
		if (isFull()) {
			throw new QueueFullException("Queue is full");
		}
		elements[tailIndex] = element;
		tailIndex = (tailIndex + 1) % capacity;
		totalElements++;
	}

	// O(1)
	public T dequeue() {
		if (isEmpty()) {
			throw new QueueEmptyException("Queue is empty");
		}
		T element = elements[headIndex];
		headIndex = (headIndex + 1) % capacity;
		totalElements--;
		return element;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("QueueImpl [capacity=");
		builder.append(capacity);
		builder.append(", elements=");
		builder.append(Arrays.toString(elements));
		builder.append(", headIndex=");
		builder.append(headIndex);
		builder.append(", tailIndex=");
		builder.append(tailIndex);
		builder.append("]");
		return builder.toString();
	}

}

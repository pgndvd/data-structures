package structures.stack;

public interface Stack<T> {

	public boolean isEmpty();

	public boolean isFull();

	public void push(T element);

	public T pop();

	public String cuteStackString();
}

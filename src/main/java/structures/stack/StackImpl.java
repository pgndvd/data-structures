package structures.stack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StackImpl<T> implements Stack<T> {
	private int capacity;
	private List<T> elements;

	public StackImpl(int capacity) {
		this.capacity = capacity;
		elements = new ArrayList<T>(capacity);
	}

	public boolean isEmpty() {
		if (elements.size() == 0) {
			return true;
		}
		return false;
	}

	public boolean isFull() {
		if (elements.size() == capacity) {
			return true;
		}
		return false;
	}

	public void push(T element) {
		if (isFull()) {
			throw new StackOverflowException("Stack overflow for element " + element);
		}
		else {
			elements.add(element);
		}
	}

	public T pop() {
		if (isEmpty()) {
			throw new StackUnderflowException("Stack underflow");
		}
		T element = elements.get(elements.size() - 1);
		elements.remove(elements.size() - 1);
		return element;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + capacity;
		result = prime * result + ((elements == null) ? 0 : elements.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StackImpl other = (StackImpl) obj;
		if (capacity != other.capacity)
			return false;
		if (elements == null) {
			if (other.elements != null)
				return false;
		}
		else if (!elements.equals(other.elements))
			return false;
		return true;
	}

	public String cuteStackString() {
		StringBuilder builder = new StringBuilder();
		int maxLenght = 0;
		for (T element : elements) {
			maxLenght = Math.max(maxLenght, element.toString().length());
		}
		String header = String.format("%0" + (maxLenght + 2) + "d", 0).replace('0', '-');
		// for (T element : elements) {
		for (int i = elements.size() - 1; i >= 0; i--) {
			T element = elements.get(i);
			builder.append(header + "\n");
			builder.append("|" + element + "|" + "\n");
		}
		builder.append(header + "\n");
		return builder.toString();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StackImpl [capacity=");
		builder.append(capacity);
		builder.append(", elements=");
		builder.append(elements);
		builder.append("]");
		return builder.toString();
	}

}

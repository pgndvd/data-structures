package structures.list.doubleLinked;

public class DoubleLinkedListImpl<T> implements DoubleLinkedList<T> {

	private ListNode<T> head;

	public DoubleLinkedListImpl(ListNode<T> head) {
		this.head = head;
	}

	// O(n)
	@Override
	public ListNode<T> search(T data) {
		ListNode<T> node = head;
		while (node != null && !node.getData().equals(data)) {
			node = node.getNext();
		}
		return node;
	}

	// O(1)
	@Override
	public ListNode<T> headInsert(T data) {
		ListNode<T> node = new ListNode<T>(data);
		return headInsert(node);
	}

	// O(1)
	@Override
	public ListNode<T> headInsert(ListNode<T> node) {
		node.setNext(head);
		if (head != null) {
			head.setPrev(node);
		}
		head = node;
		return head;
	}

	// O(n)
	@Override
	public void remove(T data) {
		remove(search(data));
	}

	// O(1)
	@Override
	public void remove(ListNode<T> node) {
		if (node.getPrev() != null) {
			node.getPrev().setNext(node.getNext());
		}
		else {
			head = node.getNext();
		}
		if (node.getNext() != null) {
			node.getNext().setPrev(node.getPrev());
		}
	}

}

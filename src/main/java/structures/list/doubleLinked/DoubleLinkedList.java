package structures.list.doubleLinked;

public interface DoubleLinkedList<T> {
	public ListNode<T> search(T data);

	public ListNode<T> headInsert(T data);

	public ListNode<T> headInsert(ListNode<T> data);

	public void remove(T data);

	public void remove(ListNode<T> data);
}

package structures.list.doubleLinked;

public class ListNode<T> {
	private ListNode<T> prev;
	private ListNode<T> next;
	private T data;

	public ListNode(T data) {
		this.data = data;
	}

	public ListNode<T> getPrev() {
		return prev;
	}

	public void setPrev(ListNode<T> prev) {
		this.prev = prev;
	}

	public ListNode<T> getNext() {
		return next;
	}

	public void setNext(ListNode<T> next) {
		this.next = next;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ListNode [prev=");
		builder.append(prev);
		// builder.append(", next=");
		// builder.append(next);
		builder.append(", data=");
		builder.append(data);
		builder.append("]");
		return builder.toString();
	}

}

package structures.tree.bst;

import java.util.Iterator;

public interface BinarySearchTree<T> {
	TreeNode<T> getRoot();
	
	Iterator<T> inorderTreeWalkIteration();

	Iterator<T> inorderTreeWalkIteration(TreeNode<T> node);

	Iterator<T> preorderTreeWalkIteration();

	Iterator<T> preorderTreeWalkIteration(TreeNode<T> node);

	Iterator<T> postorderTreeWalkIteration();

	Iterator<T> postorderTreeWalkIteration(TreeNode<T> node);

	TreeNode<T> search(T data);

	TreeNode<T> minimum();

	TreeNode<T> minimum(TreeNode<T> node);

	TreeNode<T> maximum();

	TreeNode<T> maximum(TreeNode<T> node);

	TreeNode<T> successor();

	TreeNode<T> successor(TreeNode<T> node);

	TreeNode<T> predecessor();

	TreeNode<T> predecessor(TreeNode<T> node);

	TreeNode<T> insert(T data);

	TreeNode<T> insert(TreeNode<T> node);

	TreeNode<T> delete(T data);

	TreeNode<T> delete(TreeNode<T> node);

}

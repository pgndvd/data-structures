package structures.tree.bst;

import java.util.Iterator;

import model.Person;

public class BinarySearchTreeImpl<T extends Comparable<T>>
		implements BinarySearchTree<T> {
	private TreeNode<T> root;

	public BinarySearchTreeImpl(T data) {
		this.root = new TreeNode<T>(data);
	}

	public BinarySearchTreeImpl(TreeNode<T> root) {
		this.root = root;
	}

	@Override
	public TreeNode<T> getRoot(){
		return root;
	}
	
	@Override
	public Iterator<T> inorderTreeWalkIteration() {
		return inorderTreeWalkIteration(root);
	}

	@Override
	public Iterator<T> inorderTreeWalkIteration(TreeNode<T> node) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<T> preorderTreeWalkIteration() {
		return postorderTreeWalkIteration(root);
	}

	@Override
	public Iterator<T> preorderTreeWalkIteration(TreeNode<T> node) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<T> postorderTreeWalkIteration() {
		return postorderTreeWalkIteration(root);
	}

	@Override
	public Iterator<T> postorderTreeWalkIteration(TreeNode<T> node) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TreeNode<T> search(T data) {
		return search(root, data);
	}

	private TreeNode<T> search(TreeNode<T> node, T data) {
		if (node != null) {
			System.out.println("Searching from node "+node);
			if (node.getData().equals(data)) {
				return node;
			}
			else if (node.getData().compareTo(data) < 0) {
				return search(node.getRight(), data);
			}
			else {
				return search(node.getLeft(), data);
			}
		}
		else {
			return null;
		}
	}

	@Override
	public TreeNode<T> minimum() {
		return minimum(root);
	}

	@Override
	public TreeNode<T> minimum(TreeNode<T> node) {
		while (node.getLeft() != null) {
			node = node.getLeft();
		}
		return node;
	}

	@Override
	public TreeNode<T> maximum() {
		return maximum(root);
	}

	@Override
	public TreeNode<T> maximum(TreeNode<T> node) {
		while (node.getRight() != null) {
			node = node.getRight();
		}
		return node;
	}

	/**
	 * The successor of a node x, is a node y such that y.data is the smallest of the data
	 * that are bigger than x.data
	 * @return the smallest of the bigger nodes
	 */
	@Override
	public TreeNode<T> successor() {
		return successor(root);
	}

	@Override
	public TreeNode<T> successor(TreeNode<T> node) {
		if (node != null) {
			// if node has right subtree returns minimum from there
			if (node.getRight() != null) {
				return minimum(node.getRight());
			}
			// otherwise keeps going up until the node is in the left subtree of his
			// parent
			TreeNode<T> successor = node.getParent();
			while (successor != null && node.equals(successor.getRight())) {
				node = successor;
				successor = successor.getParent();
			}
			return successor;
		}
		else {
			return null;
		}
	}

	/**
	 * The predecessor of a node x, is a node y such that y.data is the biggest of the
	 * data that are smaller than x.data
	 * @return the biggest of the smallest nodes
	 */
	@Override
	public TreeNode<T> predecessor() {
		return predecessor(root);
	}

	@Override
	public TreeNode<T> predecessor(TreeNode<T> node) {
		if (node != null) {
			// if node has left subtree returns minimum from there
			if (node.getLeft() != null) {
				return maximum(node.getLeft());
			}
			// otherwise keeps going up until the node is in the left subtree of his
			// parent
			TreeNode<T> successor = node.getParent();
			while (successor != null && node.equals(successor.getLeft())) {
				node = successor;
				successor = successor.getParent();
			}
			return successor;
		}
		else {
			return null;
		}
	}

	@Override
	public TreeNode<T> insert(T data) {
		return insert(new TreeNode<T>(data));
	}

	@Override
	public TreeNode<T> insert(TreeNode<T> node) {
		TreeNode<T> insertedParent = null;
		TreeNode<T> tmpDescending = root;
		// descends until possible
		while (tmpDescending != null) {
			insertedParent = tmpDescending;
			if (node.getData().compareTo(tmpDescending.getData()) < 0) {
				System.out.println("Going left from "+ tmpDescending);
				tmpDescending = tmpDescending.getLeft();
			}
			else {
				System.out.println("Going right from "+ tmpDescending);
				tmpDescending = tmpDescending.getRight();
			}
		}

		// fix pointers (actual insertion)
		node.setParent(insertedParent);
		System.out.println("Parent -> " + insertedParent);
		if (insertedParent == null) {
			root = node; // tree was empty
		}
		else if (node.getData().compareTo(insertedParent.getData()) < 0) {
			insertedParent.setLeft(node);
		}
		else {
			insertedParent.setRight(node);
		}
		return node;
	}

	@Override
	public TreeNode<T> delete(T data) {
		return delete(search(data));
	}

	@Override
	public TreeNode<T> delete(TreeNode<T> node) {
		System.out.println("Deleting "+ node);
		TreeNode<T> nodeToDelete = null;
		TreeNode<T> nodeToDeleteSubtreeRoot = null;
		if (node != null) {
			// if the node has 0 or 1 subtrees, the node to delete is itself, otherwise is
			// its successor
			// case 1: no subtrees
			// case 2: 1 subtree
			if (node.getLeft() == null || node.getRight() == null) {
				nodeToDelete = node;
			}
			// case 3: 2 subtrees
			else {
				nodeToDelete = successor(node);
			}

			// nodeToDeleteSubtreeRoot is the root of the subtree of the node to delete if
			// it has a subtree,
			// null otherwise
			if (nodeToDelete.getLeft() != null) {
				nodeToDeleteSubtreeRoot = nodeToDelete.getLeft();
			}
			else {
				nodeToDeleteSubtreeRoot = nodeToDelete.getRight();
			}

			// substitute the node to delete with his subtree (that has
			// nodeToDeleteSubtreeRoot as root)
			if (nodeToDeleteSubtreeRoot != null) {
				nodeToDeleteSubtreeRoot.setParent(nodeToDelete.getParent());
			}
			if (nodeToDelete.getParent() == null) {
				root = nodeToDeleteSubtreeRoot;
			}
			else if (nodeToDelete.equals(nodeToDelete.getParent().getLeft())) {
				nodeToDelete.getParent().setLeft(nodeToDeleteSubtreeRoot);
			}
			else {
				nodeToDelete.getParent().setRight(nodeToDeleteSubtreeRoot);
			}

			// if the node has 2 subtrees that key of the node is substituted with the key
			// of its successor
			if (!nodeToDelete.equals(node)) {
				node.setData(nodeToDelete.getData());
			}
		}
		return nodeToDelete;
	}

}

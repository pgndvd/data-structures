package model;

public class Person implements Comparable<Person> {
	private String name;

	public Person(String name){
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Person [name=");
		builder.append(name);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int compareTo(Person o) {
		return name.compareTo(o.name);
	}
}

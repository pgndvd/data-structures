package structures;

import org.junit.Test;

import model.Person;
import structures.tree.bst.BinarySearchTree;
import structures.tree.bst.BinarySearchTreeImpl;
import structures.tree.bst.BinaryTreePrinter;

public class BSTTest {

//	@Test
//	public void test(){
//		Person boss = new Person("boss");
//		BinarySearchTree<Person> tree = new BinarySearchTreeImpl<Person>(boss);
//		tree.insert(new Person("SW Architect"));
//		BinaryTreePrinter.printNode(tree.getRoot());
//
//		tree.insert(new Person("SW Architect"));
//		BinaryTreePrinter.printNode(tree.getRoot());
//
//		tree.insert(new Person("Senior SW Engineer"));
//		tree.insert(new Person("Senior SW Engineer"));
//		tree.insert(new Person("SW Engineer"));
//		tree.insert(new Person("SW Engineer"));
//		BinaryTreePrinter.printNode(tree.getRoot());
//		System.out.println(tree);
//	}
//	
	@Test
	public void test2(){
		BinarySearchTree<Integer> tree = new BinarySearchTreeImpl<Integer>(5);
		tree.insert(2);
		BinaryTreePrinter.printNode(tree.getRoot());

		tree.insert(6);
		BinaryTreePrinter.printNode(tree.getRoot());

		tree.insert(7);
		tree.insert(8);
		tree.insert(1);
		tree.insert(3);
		BinaryTreePrinter.printNode(tree.getRoot());
		
		tree.delete(5);
		BinaryTreePrinter.printNode(tree.getRoot());

		tree.delete(7);
		BinaryTreePrinter.printNode(tree.getRoot());

	}

}

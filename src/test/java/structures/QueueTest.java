package structures;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import model.Person;
import structures.queue.Queue;
import structures.queue.QueueEmptyException;
import structures.queue.QueueFullException;
import structures.queue.QueueImpl;

public class QueueTest {

  @Test
  public void test(){
    Queue<Person> queue = new QueueImpl<Person>(2);

    Person person1 = new Person("Giovanni");
    queue.enqueue(person1);
    System.out.println(queue);

    Person person2 = new Person("Federico");
    queue.enqueue(person2);
    System.out.println(queue);

    
    Person dequeued1 = queue.dequeue();
    Assert.assertEquals(person1, dequeued1);
    System.out.println(queue);

    
    Person dequeued2 = queue.dequeue();
    Assert.assertEquals(person2, dequeued2);
    System.out.println(queue);

    Person person3 = new Person("Rino");
    queue.enqueue(person3);
    System.out.println(queue);

  }
  
  @Test(expected = QueueFullException.class)
  public void testOverflow(){
    Queue<Person> queue = new QueueImpl<Person>(2);
    queue.enqueue(new Person("Giovanni"));
    System.out.println(queue);
    
    queue.enqueue(new Person("Federico"));
    System.out.println(queue);
    
    
    queue.enqueue(new Person("Federico"));
    System.out.println(queue);
  }
  
  @Test(expected = QueueEmptyException.class)
  public void testUnderflow(){
    Queue<Person> queue = new QueueImpl<Person>(2);
    queue.enqueue(new Person("Giovanni"));
    queue.enqueue(new Person("Federico"));
    
    Person federico = queue.dequeue();
    Person giovanni = queue.dequeue();
    Person underflow = queue.dequeue();
  }

}

package structures;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import model.Person;
import structures.stack.Stack;
import structures.stack.StackImpl;
import structures.stack.StackOverflowException;
import structures.stack.StackUnderflowException;

public class StackTest {

	@Test
	public void test(){
		Stack<Person> stack = new StackImpl<Person>(2);

		Person person1 = new Person("Giovanni");
		stack.push(person1);
		
		Person person2 = new Person("Federico");
		stack.push(person2);
		
		Person popped1 = stack.pop();
		Assert.assertEquals(person2, popped1);
		
		Person popped2 = stack.pop();
		Assert.assertEquals(person1, popped2);

	}
	
	@Test(expected = StackOverflowException.class)
	public void testOverflow(){
		Stack<Person> stack = new StackImpl<Person>(2);
		stack.push(new Person("Giovanni"));
		System.out.println(stack.cuteStackString());
		
		stack.push(new Person("Federico"));
		System.out.println(stack.cuteStackString());
		
		
		stack.push(new Person("Federico"));
		System.out.println(stack.cuteStackString());
	}
	
	@Test(expected = StackUnderflowException.class)
	public void testUnderflow(){
		Stack<Person> stack = new StackImpl<Person>(2);
		stack.push(new Person("Giovanni"));
		stack.push(new Person("Federico"));
		
		Person federico = stack.pop();
		Person giovanni = stack.pop();
		Person underflow = stack.pop();
	}

	@Test
	public void iLikeToSeeABigStack(){
		int capacity = 20;
		Stack<Person> stack = new StackImpl<>(capacity);
		for (int i = 0; i<capacity; i++){
			stack.push(new Person(UUID.randomUUID().toString()));
		}
		System.out.println(stack.cuteStackString());
	}
}
